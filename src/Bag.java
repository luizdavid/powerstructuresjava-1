/******************************************************************************
 *  Compilation:  javac Bag.java
 *  Execution:    java Bag < input.txt
 *  Dependencies: StdIn.java StdOut.java
 *
 *  A generic bag or multiset, implemented using a singly-linked list.
 *
 *  % more tobe.txt 
 *  to be or not to - be - - that - - - is
 *
 *  % java Bag < tobe.txt
 *  size of bag = 14
 *  is
 *  -
 *  -
 *  -
 *  that
 *  -
 *  -
 *  be
 *  -
 *  to
 *  not
 *  or
 *  be
 *  to
 *
 ******************************************************************************/



import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 *  The <tt>Bag</tt> class represents a bag (or multiset) of 
 *  generic items. It supports insertion and iterating over the 
 *  items in arbitrary order.
 *  <p>
 *  This implementation uses a singly-linked list with a static nested class Node.
 *  See {@link LinkedBag} for the version from the
 *  textbook that uses a non-static nested class.
 *  The <em>add</em>, <em>isEmpty</em>, and <em>size</em> operations
 *  take constant time. Iteration takes time proportional to the number of items.
 *  <p>
 *  For additional documentation, see <a href="http://algs4.cs.princeton.edu/13stacks">Section 1.3</a> of
 *  <i>Algorithms, 4th Edition</i> by Robert Sedgewick and Kevin Wayne.
 *
 *  @author Robert Sedgewick
 *  @author Kevin Wayne
 *
 *  @param <Item> the generic type of an item in this bag
 */
public class Bag<Item> implements Iterable<Item> {
    private Node<Item> first;    // beginning of bag
    private int N;               // number of elements in bag

    // helper linked list class
    private static class Node<Item> {
        private Item item;
        private Node<Item> next;
    }

    /**
     * Initializes an empty bag.
     */
    public Bag() {
        first = null;
        N = 0;
    }


 /**
    * Returns the first item in this bag. 
    * @return the first item in this bag.
    **/
    public Item first(){
         return this.first.item;
    }
    /**
     * Returns true if this bag is empty.
     *
     * @return <tt>true</tt> if this bag is empty;
     *         <tt>false</tt> otherwise
     */
    public boolean isEmpty() {
        return first == null;
    }

    /**
     * Returns the number of items in this bag.
     *
     * @return the number of items in this bag
     */
    public int size() {
        return N;
    }
    
    /**
    * Insert an item in a certain position
    * @param x - item to be inserted into the array 
    * @param position - position in which the item will be inserted
    * @throws - The method throws an exception if the list is full or the position is invalid 
    *
    * @author Vinícius Almeida
    *
    */
    
    public void insertAt(T x, int position)throws Exception {
		
		if(this.last >= this.item.length || position > this.last || position < 0)
			throw new Exception("Error: List full or invalid position!");
		
		T aux = (T) new Object();
		aux = this.item[this.last];
		int index = this.last;
		
		while (index != position){
			this.item[index] = this.item[index - 1];
			index--;
		}
		
		this.item[position] = x;
		this.last = this.last + 1;
		this.item[this.last] = aux;
	}
    
    /**
     * Adds the item to this bag.
     *
     * @param  item the item to add to this bag
     */
    public void add(Item item) {
        Node<Item> oldfirst = first;
        first = new Node<Item>();
        first.item = item;
        first.next = oldfirst;
        N++;
    }

    /**
     * Searching the list by a key. Returns the item if it is on the list, but returns null.
     *
     * @param item the item to search to this bag
     *
     * @return the item if it is on the list or null if it is not on the list
     *
     * @author Anderson Ferreira
     */
    public Item search(Item key) {
        if (this.isEmpty () || key == null) 
            return null;
        Node aux = this.first;
        while (aux != null){
            if (aux.item.equals (key)) 
                return aux.item;
            aux = aux.next;
        }
        return null;
    }
    
    /**
     * Returns an iterator that iterates over the items in this bag in arbitrary order.
     *
     * @return an iterator that iterates over the items in this bag in arbitrary order
     */
    public Iterator<Item> iterator()  {
        return new ListIterator<Item>(first);  
    }

    // an iterator, doesn't implement remove() since it's optional
    private class ListIterator<Item> implements Iterator<Item> {
        private Node<Item> current;

        public ListIterator(Node<Item> first) {
            current = first;
        }

        public boolean hasNext()  { return current != null;                     }
      
     /**
    * removes an item from the array according to a key
    * @throws - The method throws an exception if the list is empty or invalid key 
    * @param key - key assigned as parameters for the search in the array
    *
    * @return - item to be removed into the array 
    *
    * @author - Matheus Souza Carneiro
    *
    */ 
       public T remove(T key) throws Exception{
		    if(this.isEmpty() || key == null){
		   	    throw new Exception("Error! list empty or invalid key");
		    }
		    int i = 0;
		    while( i < this.last && !this.item[i].equals(key)){
		    	i++;
		    }
		    if(i >= this.last){
		    	return null;
		    }
		    T item = (T)this.item[i];
		    this.last = this.last - 1;
		    for(int aux = i; aux < this.last; aux++){
			this.item[aux] = this.item[aux+1];
		    }
		   return item;
	    }
        
    /**
    * Removes the first item in this array
    * @throws - The method throws an exception if the list is empty 
    * @return - item to be removed into the array 
    *
    * @author Carlos Victor
    *
    */ 
       public T removeFirst() throws Exception{
            if(this.isEmpty()){
			    throw new Exception("Error: List empty!");
    	    }
            T item = (T)this.item[0];
		    this.last = this.last-1;
		    for(int i = 0; i < this.last; i++){
			    this.item[i] = this.item[i + 1];
            }
		return item;
        }
             
        public Item next() {
            if (!hasNext()) throw new NoSuchElementException();
            Item item = current.item;
            current = current.next; 
            return item;
        }
    }
    
    
    /**
    *
    *this method clears the bag.
    *
    *@author Luan Dharlin
    *
    */    
        public void clean(){
            this.first = null;
            this.N=0;
        }

}

/******************************************************************************
 *  Copyright 2002-2015, Robert Sedgewick and Kevin Wayne.
 *
 *  This file is part of algs4.jar, which accompanies the textbook
 *
 *      Algorithms, 4th edition by Robert Sedgewick and Kevin Wayne,
 *      Addison-Wesley Professional, 2011, ISBN 0-321-57351-X.
 *      http://algs4.cs.princeton.edu
 *
 *
 *  algs4.jar is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  algs4.jar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with algs4.jar.  If not, see http://www.gnu.org/licenses.
 ******************************************************************************/
